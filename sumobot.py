#!/usr/bin/python2

from ev3dev.auto import *

def motors():
    ''' Notes, mostly compiled from help(LargeMotor):
        speed regulation is turned on, so please set 'speed_sp', not 'duty_cycle_sp'
        run_timed(time_sp=<time in ms>), emphasis _milliseconds_
        stop(stop_command=<Motor.stop_commands>)
          where Motor.stop_commands=[u'coast', u'brake', u'hold']
        state (read-only) can be one of [`running`, `ramping`, `holding`, `stalled`]
          where stalled would likely indicate interaction with another robot
        position (read-only), when paired with a passive 'stop' state, could 
        be used to detect being pushed from either front or back.
    '''
    # main drive motors, left=port, right=star(board)
    left_motor, right_motor = [LargeMotor(addr) for addr in (OUTPUT_B, OUTPUT_A)]
    assert left_motor.connected, "left motor is not connected to A"
    assert right_motor.connected, "right motor is not connected to B"
    
    # both motors are oriented such that 'forward' is the same.
    
    # drives the worm gear, no hard stop, 
    # will 'always' want to go in one direction (forward/positive) or be stopped. 
    # the worm gear prevents any possible backlash, so there is no need to 
    # lock the motor in place for instance
    front_motor = MediumMotor(OUTPUT_C)
    assert front_motor.connected, "front motor is not connected to C"

    # front motor 'forward' is the only direction we want, at least duty_cycle_sp of 80
    
    return left_motor, right_motor, front_motor

def sensors():
    left_light, right_light = [ColorSensor(addr) for addr in (INPUT_4, INPUT_1)]
    assert left_light.connected, "left light sensor is not connected to 4"
    assert right_light.connected, "right light sensor is not connected to 1"
    
    left_light.mode = right_light.mode = 'COL-REFLECT'

    gyro = GyroSensor(INPUT_2)
    assert gyro.connected, "gyroscope sensor is not connected to 2"
    # reset value to 0!
    gyro.mode = 'GYRO-CAL'
    gyro.mode = 'GYRO-ANG'

    return left_light, right_light, gyro
