#!/usr/bin/python2

from time import sleep,time
from moves import stop

def detect_push(leftm, rightm):
    stop(leftm, rightm, "brake", wait=True)
    sleep(0.3) # a little time to stop, this is a guess
    stop(leftm, rightm, "coast", wait=True)
    lpos_i, rpos_i = leftm.position, rightm.position
    sleep(0.1)
    stop(leftm, rightm, "brake", wait=True)
    lpos, rpos = leftm.position, rightm.position
    if lpos_i < lpos or rpos_i < rpos:
        return "PUSH_FORWARD"
    if lpos_i > lpos or rpos_i > rpos:
        return "PUSH_BACKWARD"

def detect_stall(lm, rm, duration_ms=200, stall_min=5):
    ''' motor.state never seems to reach "stall", even if held in place while the motor is engaged...
        so instead of relying on such convenience, do the opposite of "detect_push"
    '''
    # even with slow speed, should move at least n

    lpos_i, rpos_i = lm.position, rm.position
    sleep(float(duration_ms) / 1000)
    lpos, rpos = lm.position, rm.position
    if abs(lpos - lpos_i) < stall_min or abs(rpos - rpos_i) < stall_min:
        return "STALL"

def detect_edge(s, edge_value_min=48):
    '''
    Effectively, anything above 40 or so should count as 'white'/edge. ("EDGE")
    Any reading of 0-1 should be regarded as "sensor off the map" ("VOID")
    and anything between (4-10) as 'okay'/middle (None).
    VOID could be folded into EDGE, since its about the same case,
    this can be accomplished by checking for "any"/not None from this function
    '''

    v = s.value()
    if v <= 1:
        return "VOID"
    if v >= edge_value_min:
        return "EDGE"

    # python will return None here for us

def detect_edges(lefts, rights, edge_value_min=48):
    ledge = detect_edge(lefts)
    # knowing about the second detectors state might influence the reaction
    redge = detect_edge(rights)

    state = []
    if ledge:
       state.append('LEFT_'+ledge)
    if redge:
       state.append('RIGHT_'+redge)

    return tuple(state)

if __name__ == '__main__':
    from sumobot import motors, sensors
    from moves import spin, move, point_turn, moveLifter

    lm, rm, mm = motors()
    ls, rs, g = sensors()

    # for example of how to respond to said "push"
    while True:
        p = detect_push(lm, rm)
        if p:
            print "pushed!", p
            if 'FORWARD' in p:
                # try to get out of the way,
                # maybe come back at them
                move(lm, rm, speed=60, time_ms=1000)
                sleep(1)
                point_turn(lm, rm, g, -270)
            else:
                # otherwise, we're pushing
                # this is where the front wheels could be
                # activated, etc
                move(lm, rm, speed=40, time_ms=1000)
                moveLifter(mm, speed=100, time_ms=1000)
                # probably not a great idea to push forward for so short a time
                sleep(1)
        # please note the edge-checkingtealso period here is pretty bad
        # the above reactions can send the bot careening over the edge
        # before 'edge' gets checked
        e = detect_edges(ls, rs)
        if e:
            print "edge!", e
            # the most simple reaction/ motion
            # may be a bad idea if the angle to the edge is too obtuse
            move(lm, rm, speed=-60, time_ms=1000)
            sleep(1)
        move(lm, rm, speed=40, time_ms=2000)
	end = time() + 2
        while time() < end:
            if detect_stall(lm, rm):
                print "stalled!"


