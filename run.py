#!/usr/bin/python2

# perpective is from the point of view of the robot itself.
# the direction left is "port"

from time import sleep,time

from sumobot import motors, sensors
from moves import *
from senses import detect_push, detect_stall, detect_edges

# keep a bit of global state (in lieu of something more
# sophisticated that could be passed around)
# this is one of the major things we want to know
# -1 reverse, 1 forward
last_direction = 0
last_direction_swapped = False
# probably (mod 360)
last_known_deg = 0

stall_count = 0

def update_global_deg(update):
    global last_known_deg
    last_known_deg = update % 360

def main(actual_start=None, direction_picker=None, duration_picker=None):
    global last_direction
    global last_direction_swapped
    global stall_count
    start = float(actual_start)
    print "READY [%sms] ..." % (int((time() - start) * 1000)),
    first = time()
    lm, rm, mm = motors()
    ls, rs, gs = sensors()

    # heh, the actual 'start-up' time of this
    # python program + exeuction is 3-3.5 seconds anyway ... soooo

    # ~250-270 ms
    print "SET [%sms] ..." % (int((time() - start) * 1000)),
    # wait 3 seconds overall
    while time() - start < 3.00:
        pass

    print "GO! [%sms] ..." % (int((time() - start) * 1000))

    # make a move "away" first
    last_direction = 1
    move(lm, rm, time_ms=200)
    sleep(0.200)
    stop(lm, rm, "brake")

    # initial about-face
    update_global_deg(
        spin(lm, rm, gs,
            deg=random_direction() * random_range(120, 180),
            speed=100))
    stop(lm, rm, "brake")

    # (one) general idea:
    # 0 fast move for ~1 second, checking for edge or stall until time up
    # 1 stop, detect push total elapsed ~0.5 seconds
    # 2 choose + spin new random direction
    # 3 goto 0

    # pressing back button will kill the program
    # otherwise ctrl-c out if run from a shell
    while True:
        print "state: last_dir: %s, stalls: %s" % (last_direction, stall_count)
        print "choosing what to do...", 
        move_time_ms = random_range(600, 1400)
        if stall_count > 3:
            print "in a stall, decided to continue pushing dir=%s..." % last_direction
	    # continue with the "push" in whichever direction trying to head last
	    move(lm, rm, speed=last_direction*40, time_ms=move_time_ms)
            if last_direction == 1:
	        moveLifter(mm, speed=100, time_ms=5000)
        else:
            print "checking for push...", 
            push = detect_push(lm, rm)
            if push:
                print "was pushed!", push
                if 'FORWARD' in push:
                    # this is best if we haven't hit the edge "recently"
                    # try to get out of the way,
                    # come back at them
                    last_direction = 1
                    update_global_deg(
                        point_turn(lm, rm, gs,
                            deg=random_direction() * 290,
                            speed=100))
                else: #REVERSE
                    # otherwise, we're pushing forward
                    last_direction = 1
                    move(lm, rm, speed=40, time_ms=move_time_ms)
                    # keep the lifter motor going after we exit this branch
                    moveLifter(mm, speed=100, time_ms=5000)
	    else:
                # pick a random direction but keep track of which
                # way we're headed
                last_direction = direction = random_direction()
                speed = direction * 90
                print "nothing! spinning and moving dir=%s" % last_direction

                # this 'move' will return immediately, giving
                # the chance to do things like check sensors/etc
                # it can be overridden during this time by a call
                # to stop or move
                update_global_deg(
                    spin(lm, rm, gs,
                        deg=random_direction()*random_range(0, 180),
                        speed=100))
                move(lm, rm, speed=speed, time_ms=move_time_ms)

        # check for move_time_ms ms travel time for edge or stall
        end = time() + (move_time_ms / float(1000))
        while time() < end:
            if detect_stall(lm, rm):
                print "stall (%s) !" % (stall_count),
                if not last_direction_swapped:
                    last_direction *= -1
                    last_direction_swapped = True
                stall_count += 1
                if stall_count > 10:
                    stall_count = 10
            else:
                stall_count -= 1
                if stall_count < 0:
                    stall_count = 0
            edge = detect_edges(ls, rs)
            if edge:
                doedgething(lm, rm, mm, ls, rs, gs, edge)
                break

        if last_direction_swapped:
            last_direction *= -1
            last_direction_swapped = False
        print

def doedgething(lm, rm, mm, ls, rs, gs, edge):
    # edge can be (None, LEFT_EDGE, RIGHT_EDGE, LEFT_VOID, RIGHT_VOID)

    global last_direction
    if edge:
        print "edge!", edge
        # reverse direction, but don't count this as a reversal
        # reason: in backing up from edge, board isn't narrow enough to encounter
        # the "other" edge during this backup
        # also, if its an "oblique" angle, want to keep going repeatedly until edge is cleared
        move(lm, rm, speed=last_direction * -1 * 60, time_ms=800)
        sleep(0.800)

if __name__ == '__main__':
    import sys
    main(sys.argv[1])
