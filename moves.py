#!/usr/bin/python2

from time import sleep, time
from random import randint

def random_direction():
    return 1 if randint(0, 1) else -1

def random_range(low=0, high=360):
    return randint(low, high)

def speed_modulate(initial, speed):
    '''some speed modulation fanciness, so the bot slows down a little right at
       the end. completely unnecessary.
    '''
    def update(remaining):
        if remaining > 100:
            return speed
        ratio = remaining / float(initial)
        #print "ratio", ratio
        if ratio > 0.40:
            ratio = 1.0
        update_speed = speed * ratio
        if update_speed < 30:
            update_speed = 30
        elif update_speed > 100:
            update_speed = 100
        return update_speed
    return update

def spin(leftm, rightm, gyro, deg=180, speed=40):
    '''positive deg means clockwise, negative is counter-clockwise
    '''

    curr = 0
    try:
        stop(leftm, rightm, "coast", wait=True)
        curr = gyro.value()
        # about 2-8 ms to read the gyro sensor
        target = curr + deg
        print "start:", curr, "target:", target
        direction = 1 if target > curr else -1
        remaining = abs(target - curr)
        modulator = speed_modulate(remaining, speed)
        prev = 0
        stall_count = 0
        while remaining > 30:
            speed = int(modulator(remaining))
            leftm.run_direct(duty_cycle_sp=direction*speed)
            rightm.run_direct(duty_cycle_sp=-1*direction*speed)
            sleep(0.1)
            curr = gyro.value()
            remaining = abs(target - curr)
            if abs(prev - remaining) < 5:
                print "seem to be stalled (prev:", prev, "curr:", remaining,")"
                if stall_count > 4:
                    print "stalled too long, bailing"
                    break
                stall_count += 1
            else:
                stall_count = 0
            prev = remaining
            print "remaining:", remaining
        print "end:", curr
    except KeyboardInterrupt:
        pass
    stop(leftm, rightm, "brake")

    return curr

def move(lm, rm, speed=40, time_ms=None):
    if time_ms:
        lm.run_timed(duty_cycle_sp=speed, time_sp=time_ms)
        rm.run_timed(duty_cycle_sp=speed, time_sp=time_ms)
    else:
        lm.run_direct(duty_cycle_sp=speed)
        rm.run_direct(duty_cycle_sp=speed)

def moveLifter(m, speed=80, time_ms=None):
    if speed < 0:
        speed *= -1
        print "WARN lifter motor speed fixed to positive value"
    if time_ms:
        m.run_timed(duty_cycle_sp=speed, time_sp=time_ms)
    else:
        m.run_direct(duty_cycle_sp=speed)

def stop(leftm, rightm, stype="brake", wait=False):
    leftm.stop(stop_command=stype)
    rightm.stop(stop_command=stype)
    if wait:
        #print "waiting for stop"
        while any(m.state for m in (leftm, rightm)):
            print "waiting for stop, not stopped"
            sleep(0.1)
        #print "stopped"

def _modulate_with_bounds(lesser):
    higher = lesser * 1.5
    if higher >=100:
        lesser = 50
        higher = 100
    if lesser < 20:
        lesser = 20
        higher = 40

    return (lesser, higher)

def run_turn(leftm, rightm, gyro, deg=180, speed=40):
    '''positive deg means clockwise, negative is counter-clockwise
    '''
    curr = 0
    try:
        leftm_speed, rightm_speed = leftm.duty_cycle_sp, rightm.duty_cycle_sp
        _min = min(leftm_speed, rightm_speed)
        if speed < _min:
            speed = _min
        curr = gyro.value()
        # about 2-8 ms to read the gyro sensor
        target = curr + deg
        print "start:", curr, "target:", target
        if target > curr:
            direction = 1
            right_speed, left_speed =  _modulate_with_bounds(speed)
        else:
            direction = -1
            left_speed, right_speed =  _modulate_with_bounds(speed)
        remaining = abs(target - curr)
        prev = 0
	stall_count = 0
        while remaining > 5:
            leftm.run_direct(duty_cycle_sp=left_speed)
            rightm.run_direct(duty_cycle_sp=right_speed)
            sleep(0.1)
            curr = gyro.value()
            remaining = abs(target - curr)
            if abs(prev - remaining) < 2:
                print "seem to be stalled (prev:", prev, "curr:", remaining,")"
                if stall_count > 4:
                    print "stalled too long, bailing"
                    break
                stall_count += 1
            else:
                stall_count = 0
            prev = remaining
            print "remaining:", remaining
        print "done:", curr
    except KeyboardInterrupt:
        pass
    stop(leftm, rightm, "brake")

    return curr

def point_turn(leftm, rightm, gyro, deg=180, speed=40):
    '''positive deg means clockwise, negative is counter-clockwise
    '''
    curr = 0
    try:
        leftm_speed, rightm_speed = leftm.duty_cycle_sp, rightm.duty_cycle_sp
        _min = min(leftm_speed, rightm_speed)
        if speed < _min:
            speed = _min
        curr = gyro.value()
        # about 2-8 ms to read the gyro sensor
        target = curr + deg
        print "start:", curr, "target:", target
        if target > curr:
            direction = 1
            right_speed, left_speed =  0, _modulate_with_bounds(speed)[-1]
            rightm.stop(stop_command="brake")
        else:
            direction = -1
            left_speed, right_speed =  0, _modulate_with_bounds(speed)[-1]
            leftm.stop(stop_command="brake")
        remaining = abs(target - curr)
        prev = 0
	stall_count = 0
        while remaining > 15:
            leftm.run_direct(duty_cycle_sp=left_speed)
            rightm.run_direct(duty_cycle_sp=right_speed)
            sleep(0.1)
            curr = gyro.value()
            remaining = abs(target - curr)
            if abs(prev - remaining) < 2:
                print "seem to be stalled (prev:", prev, "curr:", remaining,")"
                if stall_count > 4:
                    print "stalled too long, bailing"
                    break
                stall_count += 1
            else:
                stall_count = 0
            prev = remaining
            print "remaining:", remaining
        print "done:", curr
    except KeyboardInterrupt:
        pass
    stop(leftm, rightm, "brake")

    return curr

if __name__ == '__main__':
    from sumobot import motors, sensors
    lm, rm, mm = motors()
    ls, rs, g = sensors()
    #print "forward 2 seconds"
    #move(lm, rm)
    #sleep(2)
    #print "spin left 200"
    #spin(lm, rm, g, -200, 100)
    #print "move 1 second"
    #move(lm, rm, speed=20)
    #sleep(1)
    #print "spin right 90"
    #spin(lm, rm, g, 90, 100)

    #run_turn(lm, rm, g, 90)
    #move(lm, rm, speed=80)
    #sleep(1)
    #run_turn(lm, rm, g, -90)
    point_turn(lm, rm, g, -90)

    while True:
        stop(lm, rm, "coast", wait=True)
        sleep(0.5)
