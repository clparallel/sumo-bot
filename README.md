# Setup

ev3 Lego robot for the sumo competition.

## OS, Software
 * debian linux
 * ev3dev http://www.ev3dev.org/
 * python driver bindings

## Hardware

### Overall
This bot tries to be low to the ground, presenting the least gap for flipping arms. The low center of gravity also makes it better at getting the advantage over taller robots when trying to push or disable them. Two large wheels suspended off the ground attached to the same worm gear driven by a medium motor is the main offensive component. When pushing against another bot, these wheels should be turned so they are lifting the other bot up or repelling an arm that is trying to flip it.

### Drive
Two large motors directly drive two medium tires near the midpoint of the body. A ball-bearing encased in a socket makes the third wheel in the center back of the body.

### Sensors
Two color/light sensors are placed near the main wheels, one on either side, each set just behind the wheels on the outside. In reflection mode, they can sense the black area vs the white warning track at all angles that allow detection of the edge before the rest of the bot would start to tip.

A gyroscope sensor (used to detect rotation) is along the centerline of the wheels. It can be checked for "completion" of a turn if needed.

The distance sensor could be added as needed to detect another robot close by.
